module.exports = function(grunt) {
    'use strict';

    grunt.initConfig({
        autoprefixer: {            
            dist: {
               files: {
                    'deploy/css/layout.css': 'source/css/layout.css', 
                    'deploy/css/mobile.css': 'source/css/mobile.css', 
               },
            },
        },

        copy: {
            dist: {
                files: [
                    {
                        expand: true, //habilita o cwd
                        cwd: 'source/',    //relativo à source, mas não a inclui na cópia          
                        src: 'vendor/**/**/**', 
                        dest: 'deploy/'
                    },
                    {
                        expand: true, //habilita o cwd
                        cwd: 'source/',    //relativo à source, mas não a inclui na cópia          
                        src: 'img/*', 
                        dest: 'deploy/'
                    },
                    {
                        expand: true, //habilita o cwd
                        cwd: 'source/',
                        src: 'favicon.png', 
                        dest: 'deploy/'
                    },
                    {
                        expand: true, //habilita o cwd
                        cwd: 'source/',
                        src: 'index.html', 
                        dest: 'deploy/'
                    },
                    {
                        expand: true, //habilita o cwd
                        cwd: 'source/',
                        src: 'temp.html', 
                        dest: 'deploy/'
                    }
                ]        
           }
        },

        clean: {
              dist: {
                src: ["deploy"]
              }
        },

        cssmin: {
            dist: {
             files: {
                  'deploy/css/layout.css': 'deploy/css/layout.css', 
                  'deploy/css/mobile.css': 'deploy/css/mobile.css',
               }
            }
        },

        uglify: {
            options: {
              mangle: true
            },

            dist: {
              files: {
                'deploy/js/main.js': [
                    'source/js/main.js'
                ]
              }
            },
        }


    });


    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('deploy', ['clean', 'autoprefixer', 'cssmin', 'uglify', 'copy'])


}