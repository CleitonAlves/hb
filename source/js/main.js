jQuery(document).ready(function($) {
    
    var screensize = $(window).width();
    var headerHeight = $('#nav').outerHeight();

	/*-------------------------------------------------------------
	  Fade Out/In Navigation
    -------------------------------------------------------------*/   
    /* $(window).scroll(function(){
        if($(window).scrollTop() >= $(window).height()-headerHeight){
            $('#nav').addClass('opaque').fadeIn();
        }else if($(window).scrollTop() == 0){
            $('#nav').removeClass('opaque').fadeIn();
        }else{
            $('#nav').removeClass('opaque').fadeOut();
        }
    }); */

    /*------------------------------------------------------------- 
      Hide Mobile Menu After Click
    -------------------------------------------------------------*/ 
    $('#menu a').on('click', function(e) {
        $('#menu.show').collapse('hide');
    });

	/*-------------------------------------------------------------
	  Smooth Scroll
	-------------------------------------------------------------*/
	$('a[href*=#]:not([href=#])').bind('click',function () {
        var hash    = this.hash;
        var idName  = hash.substring(1);    // get id name
        var alink   = this;                 // this button pressed

        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                var offset = target.offset().top - headerHeight;
                $('html,body').animate({
                    scrollTop: offset
                }, 1200);
                return false;
            }
        }
    });

    /*-------------------------------------------------------------
	  Modal Bootstrap
	-------------------------------------------------------------*/
	$('.modal').on('show.bs.modal', function(){
		var scrollX = window.scrollX, scrollY = window.scrollY;
		window.onscroll = function(e){
		    scroll(scrollX,scrollY);
		}
    });

	$('.modal').on('hidden.bs.modal', function(){
		window.onscroll = '';
    });

    /* Adicionar - Ampliar */
    $('#adicionar').click(function(){
        $('#adicionais').animate({
            top: 0,
        }, 500);
    })
    $('#adicionais .fechar').click(function(){
        $('#adicionais').animate({
            top: '-100%',
        }, 500)
    });
    $('#ampliar').click(function(){
        $('#ampliado').animate({
            left: 0,
        }, 500);
    })
    $('#ampliado .fechar').click(function(){
        $('#ampliado').animate({
            left: '100%',
        }, 500)
    });

    /*-------------------------------------------------------------
      Form List Building
    -------------------------------------------------------------*/
    $("#form-news").on('submit', function(event) {
        event.preventDefault(); 
        var formData = $(this).serialize();
        $("#feedback").empty();
        $.ajax({
            type: 'POST',
            url: 'mail.php',
            dataType: "json",
            data: formData,
            success: function(response) { 
                $("#feedback").html(response.message);
            },
            error: function(xhr, status, error){
                $("#feedback").html(response.message);
            }
        });
    });

});