<?php 
header('Content-type: application/json');

$status = array(
    'type'=>'success',
    'message'=>'Obrigado pela sua mensagem.'
);

$email = @trim(stripslashes($_POST['email']));

if(isset($email)){
    $to      = 'cleiton@criacaoparaweb.com.br';
    $subject = "HB Site - Novo Cadastro de E-mail"; 
    $message = "Novo e-mail cadastrado: ".$email; 
    
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "Reply-To: ".$email."\r\n"; 
    $headers .= "From: cleiton@criacaoparaweb.com.br\r\n"; 
    
    
    if(mail($to, $subject, $message, $headers)) {
        echo json_encode($status); 
    } else {
        $status = array(
            'type'=>'error',
            'message'=>'ERRO! Mensagem não enviada.'
        );
        echo json_encode($status);
    }
    exit;
 }


?>